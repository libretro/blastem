LOCAL_PATH := $(call my-dir)

CORE_DIR := $(LOCAL_PATH)/..

SOURCES_C := \
	$(CORE_DIR)/68kinst.c \
	$(CORE_DIR)/arena.c \
	$(CORE_DIR)/backend.c \
	$(CORE_DIR)/backend_x86.c \
	$(CORE_DIR)/config.c \
	$(CORE_DIR)/debug.c \
	$(CORE_DIR)/gdb_remote.c \
	$(CORE_DIR)/gen.c \
	$(CORE_DIR)/genesis.c \
	$(CORE_DIR)/gen_x86.c \
	$(CORE_DIR)/gst.c \
	$(CORE_DIR)/hash.c \
	$(CORE_DIR)/i2c.c \
	$(CORE_DIR)/io.c \
	$(CORE_DIR)/jcart.c \
	$(CORE_DIR)/libblastem.c \
	$(CORE_DIR)/m68k_core.c \
	$(CORE_DIR)/m68k_core_x86.c \
	$(CORE_DIR)/megawifi.c \
	$(CORE_DIR)/mem.c \
	$(CORE_DIR)/multi_game.c \
	$(CORE_DIR)/net.c \
	$(CORE_DIR)/nor.c \
	$(CORE_DIR)/paths.c \
	$(CORE_DIR)/psg.c \
	$(CORE_DIR)/realtec.c \
	$(CORE_DIR)/rom.db.c \
	$(CORE_DIR)/romdb.c \
	$(CORE_DIR)/saves.c \
	$(CORE_DIR)/sega_mapper.c \
	$(CORE_DIR)/serialize.c \
	$(CORE_DIR)/sms.c \
	$(CORE_DIR)/system.c \
	$(CORE_DIR)/terminal.c \
	$(CORE_DIR)/tern.c \
	$(CORE_DIR)/util.c \
	$(CORE_DIR)/vdp.c \
	$(CORE_DIR)/wave.c \
	$(CORE_DIR)/xband.c \
	$(CORE_DIR)/ym2612.c \
	$(CORE_DIR)/z80inst.c \
	$(CORE_DIR)/z80_to_x86.c

COREFLAGS := -std=gnu99 -DINLINE=inline -D__LIBRETRO__

GIT_VERSION := " $(shell git rev-parse --short HEAD || echo unknown)"
ifneq ($(GIT_VERSION)," unknown")
	COREFLAGS += -DGIT_VERSION=\"$(GIT_VERSION)\"
endif

include $(CLEAR_VARS)
LOCAL_MODULE    := retro
LOCAL_SRC_FILES := $(SOURCES_C)
LOCAL_CFLAGS    := $(COREFLAGS)
include $(BUILD_SHARED_LIBRARY)

%.db.c : %.db
	sed $< -e 's/"/\\"/g' -e 's/^\(.*\)$$/"\1\\n"/' -e'1s/^\(.*\)$$/const char $(shell echo $< | tr '.' '_')_data[] = \1/' -e '$$s/^\(.*\)$$/\1;/' > $@

